import json
import os


class JsonFiles:
    def __init__(self, data, file_name):
        self.file_name = file_name
        self.data = data

    def write_to_file(self):
        with open('{}{}{}'.format('D:/', self.file_name, '.json'), "w") as write_file:
            json.dump(self.data, write_file, indent=4)

    def reading_files(self):
        with open('{}{}{}'.format('D:/', self.file_name, '.json'), "r") as read_file:
            print(json.load(read_file))

    def data_aggregation(self, file1='data_person', file2='data_student'):
        self.file1 = file1
        self.file2 = file2
        with open('{}{}{}'.format('D:/', self.file1, '.json'), "r") as read_file:
            data_file1 = json.load(read_file)
        with open('{}{}{}'.format('D:/', self.file2, '.json'), "r") as read_file:
            data_file2 = json.load(read_file)

        """by creating list"""
        with open('D:/new_file_2.json', "w") as write_file:
            json.dump([data_file1, data_file2], write_file, indent=4)

        """by update dict"""
        data_file1.update(data_file2)
        with open('D:/new_file.json', "w") as write_file:
            json.dump(data_file1, write_file, indent=4)

    def file_path(self):
        print(os.path.realpath(self.file_name))
        print(os.path.abspath(self.file_name))


data_person = {
    "person": {
        "first name": "Ivan",
        "last name": "Ivanov",
        "age": "20"
    }
}
data_student = {
    "student": {
        "university": "European University",
        "faculty": "Management",
        "specialty": [
            {
                "title": "Organisation management",
                "teacher": [
                    {
                        "first name": "Petr",
                        "last name": "Petrov"
                    }
                ]
            }
        ]
    }
}
a = JsonFiles(data_person, 'data_person')
b = JsonFiles(data_student, 'data_student')
a.write_to_file()
b.write_to_file()
a.reading_files()
a.data_aggregation()
a.file_path()

