class Person:

    def __init__(self, name, profession, salary=10000):
        self.name = name
        self.profession = profession
        self.salary = salary
        self.period = 1
        self.bonus = None
        self.percent = 1
        self.extra_bonus = 0

    def work_experience_bonus(self, period):
        if period > 5:
            self.percent = period
        return self.percent

    def calculation_pay(self, period):
        self.percent = self.work_experience_bonus(period)
        self.bonus = int(self.salary * self.percent / 100)
        self.salary += self.bonus
        return self.salary

    def __str__(self):
        return '{}{}{}{}{}'.format(self.name, ' works as a ', self.profession, '. Salary - ', self.salary)


class Manager(Person):
    def __init__(self, name, salary):
        Person.__init__(self, name, 'manager', salary)

    def calculation_pay(self, period, extra_bonus=1000):
        self.extra_bonus = extra_bonus
        self.salary = Person.calculation_pay(self, period) + extra_bonus
        return self.salary


alex = Person('Alex', 'driver')
print(alex)
alex.calculation_pay(10)
print(alex)

maria = Manager('Maria', 15000)
print(maria)
maria.calculation_pay(5)
print(maria)

print(maria.salary, maria.bonus, maria.extra_bonus)
