class CheckSymbols:
    def __init__(self, symbols=input()):
        self.symbols = symbols
        self.pattern = '()[]{}<>'
        self.pattern_dict = {')': '(', ']': '[', '}': '{', '>': '<'}

    def checks_correct_input(self):
        for i in self.symbols:
            if i not in self.pattern:
                print('incorrect input data!!')
                break

    def checks_correct_combination(self):
        s, n = [], 0
        for i in self.symbols:
            if i in self.pattern:
                if i in self.pattern_dict.values():
                    s.append(i)
                    n += 1
                elif len(s) != 0 and self.pattern_dict[i] == s[-1]:
                    s.pop(-1)
                    n += 1
                else:
                    print('this is a wrong combination!!!')
                    break

        if n == len(self.symbols):
            if len(s) == 0:
                print('\N{grinning face with smiling eyes} All of combinations are correct !!!')
            else:
                print('this is a wrong combination!!!')


a = CheckSymbols()
a.checks_correct_input()
a.checks_correct_combination()
